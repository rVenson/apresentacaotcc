---
marp: true
theme: cedup
style: |
    header, footer {
    }
    section.lead {
        background: linear-gradient(#222, #292929);
    }
    section{
        background-image: url("slidebackground.png");
    }
    h6 {
    font-size: 0.7em;
    }
    blockquote {
        font-size: 0.7em;
    }
    th {
        background-color: #333;
        color: #FFF;
    }
class:
  - lead
  - invert
---

<!-- 
header: Centro de Educação Profissional Abílio Paulo
footer: Projeto Integrado de Desenvolvimento do Curso de Ensino Médio Integrado em Informática 
-->

![bg right:35% sepia](img/background.jpg)

# Título do meu projeto
## Descrição do meu projeto

Estudante 1
Estudante 2

###### *Criciúma, Novembro de 2019*

---

<!--
paginate: true
class:
-->

## Agenda

1. Introdução e Justificativa
1. Projeto de Software
    1. Análise de Requisitos
    1. Modelagem de Dados
1. Software Desenvolvido
    1. Linguagens e Persistência de Dados
    1. Frameworks, Ferramentas e Recursos
1. Demonstração
1. Conclusão e Agradecimentos

---

## Introdução

![bg right:30%](img/background.jpg)

> Introduzir todos os temas que fizeram parte do TCC. Explicar de onde surgiu a ideia. Falar sobre a área de aplicação.

* Tópico 1
* Tópico 2
* Tópico 3
* Tópico 4

---

## Justificativa

> Explicar porque o tema é importante para a sociedade? Como funciona sem o uso de tecnologia e como pretende-se melhorar a vida do usuário?

![bg right:30%](img/background.jpg)

* Tópico 1
* Tópico 2
* Tópico 3
* Tópico 4

---

## Projeto de Software

![bg right:50%](img/background.jpg)

---

### Análise de Requisitos

Requisitos Funcionais

| Requisito | Descrição |
| -- | -- |
| Tela de Login | Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.|
| Tela de Login | Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.|
| Tela de Login | Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.|

---

### Análise de Requisitos

Requisitos Não-Funcionais

| Requisito | Descrição |
| -- | -- |
| Banco de Dados | Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. |
| Banco de Dados | Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. |
| Banco de Dados | Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. |

---

### Modelagem de Dados

![bg right:50%](img/background.jpg)

---

#### Diagrama de Caso de Uso

![bg left:70% fit](img/casouso.png)

---

#### Diagrama de Classe

![bg right:70% fit](img/classe.png)

---

## Software Desenvolvido

![bg right:60%](img/softwaredesenvolvido.jpg)

---

### Linguagens Usadas

![bg left:30% fit](img/linguagem02.png)
![bg left:30% fit](img/linguagem01.png)
![bg left:30% fit](img/linguagem03.png)

* HTML
* CSS
* JavaScript

---

### Persistência de Dados

![bg left:30% fit](img/banco.png)

* MySQL

---

### Frameworks e Ferramentas

![bg left:30% fit](img/ferramenta.png)
![bg left:30% fit](img/framework.jpg)

* VSCode
* Bootstrap

---

### Recursos Utilizados

![bg right:30%](img/background.jpg)

* Computador
* Smartphone
* Tempo

---

## Testes

> Foram realizados testes? Como procederam? Qual foi o impacto?

![bg right:30%](img/background.jpg)

---

# Demonstração

---

## Conclusões

![bg right:30%](img/background.jpg)

> Falar sobre os principais aprendizados adquiridos no decorrer no projeto. Quais foram as principais dificuldades?

* Tópico 1
* Tópico 2
* Tópico 3
* Tópico 4

---

## Projetos Futuros

![bg right:30%](img/background.jpg)

> Recomendar extensões do projeto

* Tópico 1
* Tópico 2
* Tópico 3

---

## Agradecimentos

![bg right:40% fit](img/aluno01.jpg)
![bg vertical fit](img/aluno02.jpg)

>Agradecer aos colegas e familiares com colaboraram com o trabalho.

---

![bg left:30% fit](img/cedup.png)

# Obrigado pela atenção!
## Dúvidas?

Estudante 01
Estudante 02

> *Todas as imagens usadas nesta apresentação são de domínio público, dos autores do trabalho ou propriedade de suas respectivas marcas*